<?php
if ($_POST['slotA']=="" && $_POST['slotB']=="" && $_POST['slotC']=="") {header("Location: form.php");}
?>
<!DOCTYPE html>
<html>
<head>
	<title>mak8</title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/css/bootstrap.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<style>
	td:first-child{text-align: left; }
	td{text-align: center;}
	</style>
	<script>
	function collectThrow() {
		var cname;
		for (var i = 0; i < 46; i++) {
			cname = "ent"+i;
			dname = "sav"+i;
			var temp = document.getElementById(cname).innerHTML;
			document.getElementById(dname).value = temp;
			fnamo.submit();
		}
	};
	</script>
</head>
<body><div class="container-fluid">
<?php
//Generating whole table with something
//tr -> td : Generating a 5*9 matrix
$day=["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"];
$slotArr[11]=$slotArr[32]=$slotArr[53]=$_POST['slotA'];//="DataBase Management";
$slotArr[21]=$slotArr[14]=$slotArr[42]=$_POST['slotB'];//="Theory of Computation";
$slotArr[31]=$slotArr[24]=$slotArr[52]=$_POST['slotC'];//="Operating System";
$slotArr[13]=$slotArr[34]=$slotArr[41]=$_POST['slotD'];
$slotArr[23]=$slotArr[44]=$slotArr[51]=$_POST['slotE'];//="Signals and Systems";
$slotArr[12]=$slotArr[33]=$slotArr[54]=$_POST['slotF'];
$slotArr[22]=$slotArr[43]=$slotArr[15]=$_POST['slotG'];//=" ";
$slotArr[19]=$slotArr[29]=$slotArr[35]=$_POST['slotH'];//="Web Programming";
$slotArr[16]=$slotArr[17]=$slotArr[18]=$_POST['slotP'];
$slotArr[26]=$slotArr[27]=$slotArr[28]=$_POST['slotQ'];//="DBMS Lab";
$slotArr[36]=$slotArr[37]=$slotArr[38]=$_POST['slotR'];//="Programming Lab";
$slotArr[46]=$slotArr[47]=$slotArr[48]=$_POST['slotS'];
$slotArr[56]=$slotArr[57]=$slotArr[58]=$_POST['slotT'];
echo "<h2>Your TimeTable is Ready</h2><hr>";
echo "<p>You Can Still edit here, press Submit when done.</p>";
echo "<table class='table table-bordered table-responsive'>";
echo "<thead><tr><th>#</th><th>8:00-9:00</th><th>9:00-10:00</th><th>10:15-11:15</th><th>11:15-12:15</th><th>1:00-2:00</th><th>2:00-5:00</th><th>5:00-6:00</th></tr></thead><tbody>";
$count=0;
$count2=0;
for($row=1; $row<6; $row++){
	echo "<tr>\n";
	echo "<td>$day[$count]</td>\n";
	for($col=1; $col<8; $col++){
		$n = $row.$col;
		if (!isset($slotArr[$n]) || $slotArr[$n]==""|| $slotArr[$n]==" ") {echo "<td contenteditable class='active'  id=\"ent$count2\">&nbsp;</td>\n";}
		else{echo "<td contenteditable id=\"ent$count2\">$slotArr[$n]</td>\n";}
		$count2++;
	}
	echo "</tr>\n";
	$count++;
}
echo "</tbody></table>";
echo "<form action='index.php' method='POST' name='fnamo'>";
for ($i=0; $i < 36; $i++) { 
	echo "<input type='hidden' name=\"val$i\" id=\"sav$i\" />";
}
echo "<input type='button' class='btn btn-primary' value='Submit' onclick='collectThrow()' />";
echo "</form>";
//What to do now for editing purpose ?
//Submit button collect all data from given ids and put it to hidden textfields and then this.submit() to submit to another page
//The another page reads all names array and print according to it! Done!!! Export it! include a desinging file. 
//Desinging to set the css of different timetables present and a toolbar for exporting.
//Exporting??? PrintScr, SaveAs, 
?>
</div>